import React from 'react';

const SquareWithColor = ({color, classNames}) => (
    <div className={`${classNames || ''}`}  style={{background: color}} />
);

export default SquareWithColor;