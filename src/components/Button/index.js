import React from 'react';

const Button = ({ name, onActive }) => (
  <button onClick={onActive}>{name}</button>
);

export default Button;
