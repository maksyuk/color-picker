import React from 'react';
import SquareWithColor from '../SquareWithColor';

import './list-colors.css';

const ListColors = ({ list, onActive }) =>
  list.map(({ name, value }) => (
    <div className="list-color__item" onClick={() => onActive(value)} key={name}>
      <div>{name.toUpperCase()}</div>
      <SquareWithColor color={value} classNames={'item-list__square'} />
    </div>
  ));

export default ListColors;
