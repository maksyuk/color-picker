import React from 'react';

class SquareChoise extends React.Component {
  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside = e => {
    if (
      this.wrapper &&
      !e.target.parentNode.parentNode.contains(this.wrapper)
    ) {
      this.props.onClose();
    }
  };

  render() {
    return <div ref={el => (this.wrapper = el)}>{this.props.children}</div>;
  }
}

export default SquareChoise;
