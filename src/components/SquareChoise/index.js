import React from 'react';

class SquareChoise extends React.Component {
  render() {
    const {
      children,
      classNames,
      type,
      classNameDropdown,
      onSwitch,
      isOpenDropdown,
    } = this.props;

    return (
      <div>
        <div className={`square-choise ${classNames || ''}`} onClick={onSwitch}>
          {isOpenDropdown && <div className="tooltip" id={'tooltip'} />}
          {type}
        </div>
        {isOpenDropdown && (
          <div className={`dropdown ${classNameDropdown || ''}`}>
            {children}
          </div>
        )}
      </div>
    );
  }
}

export default SquareChoise;
