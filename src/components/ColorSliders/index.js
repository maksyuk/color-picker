import React, { Component } from 'react';
import RangeInput from '../RangeInput';
import Button from '../Button'
import { rgbToHex } from '../../utils';
import './color-sliders.css';

class ColorSliders extends Component {
  state = {
    red: 255,
    green: 0,
    blue: 0,
  };

  onHandleDrag = color => e => {
    this.setState({ [color]: +e.target.value }, () => {
      this.onActive();
    });
  };

    onActive = () => {
    const { red, green, blue } = this.state;
    const hex = rgbToHex(red, green, blue);
    this.props.onActive(hex);
  };

  render() {
    const { red, green, blue } = this.state;
    const { onAccept, onCancel } = this.props;
    return (
      <React.Fragment>
        <RangeInput
          name="R"
          value={red}
          color="red"
          onDrag={this.onHandleDrag}
        />
        <RangeInput
          name="G"
          value={green}
          color="green"
          onDrag={this.onHandleDrag}
        />
        <RangeInput
          name="B"
          value={blue}
          color="blue"
          onDrag={this.onHandleDrag}
        />
        <div className="button-group">
            <Button name={'CANCEL'} onActive={onCancel}/>
            <Button name={'OK'} onActive={onAccept}/>
        </div>
      </React.Fragment>
    );
  }
}

export default ColorSliders;
