import React from 'react';

const InputHex = ({ color }) => (
  <input className="input-hex" type="text" disabled={true} value={color} />
);

export default InputHex;
