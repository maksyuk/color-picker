export const ARROW_ICON_LINK = 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-512.png';
export const LIST_COLORS = [
    {name: 'red', value: '#ff0000'},
    {name: 'green', value: '#008000'},
    {name: 'blue', value: '#0000ff'},
    {name: 'yellow', value: '#ffff00'}
];
