import React, { Component } from 'react';
import './App.css';
import InputHex from './components/InputHex';
import SquareChoise from './components/SquareChoise';
import SquareWithColor from './components/SquareWithColor';
import ListColors from './components/ListColors';
import ColorSliders from './components/ColorSliders';
import CloseDropDown from './components/SquareChoise/CloseDropDown';
import { ARROW_ICON_LINK, LIST_COLORS } from './constants';

class App extends Component {
  state = {
    activeColor: '#009999',
    tempColor: '#009999',
    isOpenSlider: false,
    isOpenListColors: false,
  };

  onActiveColor = value => {
    this.setState({ tempColor: value });
  };

  onSwitchIsOpeniSlider = () =>
    this.setState({ isOpenSlider: !this.state.isOpenSlider });

  onSwitchIsOpenListColors = () =>
    this.setState({ isOpenListColors: !this.state.isOpenListColors });

  onAcceptColor = value => {
    this.setState({ activeColor: value });
  };

  onActiveColorList = value => {
    this.onActiveColor(value);
    this.onAcceptColor(value);
    this.onSwitchIsOpenListColors();
  };

  onAcceptColorSliders = () => {
    this.onAcceptColor(this.state.tempColor);
    this.onSwitchIsOpeniSlider();
  };

  onCancelColor = () => {
    this.setState({ tempColor: this.state.activeColor });
    this.onSwitchIsOpeniSlider();
  };

  render() {
    const {
      activeColor,
      tempColor,
      isOpenSlider,
      isOpenListColors,
    } = this.state;
    const typeColor = (
      <SquareWithColor classNames="input-color" color={tempColor} />
    );
    const typeArrow = (
      <img src={ARROW_ICON_LINK} alt="arrow" width="100%" height="100%" />
    );
    return (
      <div className="App">
        <div className="color-picker">
          <InputHex color={activeColor} onActive={this.onActiveColor} />
          <SquareChoise
            onSwitch={this.onSwitchIsOpeniSlider}
            isOpenDropdown={isOpenSlider}
            type={typeColor}
            classNames="square-block-color"
          >
            <CloseDropDown onClose={this.onSwitchIsOpeniSlider}>
              <ColorSliders
                onActive={this.onActiveColor}
                onAccept={this.onAcceptColorSliders}
                onCancel={this.onCancelColor}
              />
            </CloseDropDown>
          </SquareChoise>
          <SquareChoise
            onSwitch={this.onSwitchIsOpenListColors}
            isOpenDropdown={isOpenListColors}
            type={typeArrow}
            classNames="square-block-color"
          >
            <CloseDropDown onClose={this.onSwitchIsOpenListColors}>
              <ListColors
                list={LIST_COLORS}
                onActive={this.onActiveColorList}
              />
            </CloseDropDown>
          </SquareChoise>
        </div>
      </div>
    );
  }
}

export default App;
